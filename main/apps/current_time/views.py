# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, HttpResponse, redirect
from time import gmtime, strftime, localtime

def index(request):
    context = {
        'time' : strftime("%a, %d %b %Y %I:%M:%S", localtime())
    }
    return render(request, 'current_time/index.html', context)
